# frozen_string_literal: true

RSpec.describe Gcs::Util do
  let(:report) { fixture_file_json_content('report.json') }
  let(:allow_list) { Gcs::AllowList.new(fixture_file('image-allowlist.yml')) }

  describe 'writes file to given location' do
    let(:tmp_dir) { Dir.mktmpdir }
    let(:full_path) { Pathname.new(tmp_dir).join(Gcs::DEFAULT_REPORT_NAME) }

    subject(:write_file) do
      described_class.write_file(Gcs::DEFAULT_REPORT_NAME, report, Gcs::Environment.project_dir, allow_list)
    end

    around do |example|
      with_modified_environment 'CI_PROJECT_DIR' => tmp_dir do
        write_file
        example.run
        FileUtils.remove_entry tmp_dir
      end
    end

    context 'without allow list' do
      let(:allow_list) { nil }

      specify do
        expect(fixture_file_content(full_path)).to match(/CVE-2019-3462/)
      end
    end

    context 'with allow list' do
      specify do
        expect(fixture_file_content(full_path)).not_to match(/CVE-2019-3462/)
      end
    end

    shared_examples 'with file content' do
      specify do
        expect(fixture_file_content(full_path)).not_to be_empty
      end
    end

    context 'when content is hash' do
      it_behaves_like 'with file content'
    end

    context 'when content is json' do
      let(:report) { fixture_file_content('report.json') }

      it_behaves_like 'with file content'
    end
  end

  describe '.write_table' do
    subject(:write_table) { described_class.write_table(report, allow_list) }

    let(:expected_output) do
      <<~TEXT
      +----------+-----------------------+--------------+-----------------+------------------------------------------------------------------------+
      |  STATUS  |     CVE SEVERITY      | PACKAGE NAME | PACKAGE VERSION |                            CVE DESCRIPTION                             |
      +----------+-----------------------+--------------+-----------------+------------------------------------------------------------------------+
      | \e[32mApproved\e[0m |  High CVE-2019-3462   |     apt      |      1.4.8      | Incorrect sanitation of the 302 redirect field in HTTP transport metho |
      |          |                       |              |                 | d of apt versions 1.4.8 and earlier can lead to content injection by a |
      |          |                       |              |                 |  MITM attacker, potentially leading to remote code execution on the ta |
      |          |                       |              |                 |                             rget machine.                              |
      +----------+-----------------------+--------------+-----------------+------------------------------------------------------------------------+
      | \e[32mApproved\e[0m | Medium CVE-2020-27350 |     apt      |      1.4.8      | APT had several integer overflows and underflows while parsing .deb pa |
      |          |                       |              |                 | ckages, aka GHSL-2020-168 GHSL-2020-169, in files apt-pkg/contrib/extr |
      |          |                       |              |                 | acttar.cc, apt-pkg/deb/debfile.cc, and apt-pkg/contrib/arfile.cc. This |
      |          |                       |              |                 |  issue affects: apt 1.2.32ubuntu0 versions prior to 1.2.32ubuntu0.2; 1 |
      |          |                       |              |                 | .6.12ubuntu0 versions prior to 1.6.12ubuntu0.2; 2.0.2ubuntu0 versions  |
      |          |                       |              |                 | prior to 2.0.2ubuntu0.2; 2.1.10ubuntu0 versions prior to 2.1.10ubuntu0 |
      |          |                       |              |                 |                                  .1;                                   |
      +----------+-----------------------+--------------+-----------------+------------------------------------------------------------------------+
      TEXT
    end

    it 'outputs data as a table' do
      expect { write_table }.to output(expected_output).to_stdout
    end

    context 'without allow list' do
      let(:allow_list) { nil }

      specify do
        expect { write_table }.to output(/unapproved/i).to_stdout
      end
    end

    context 'with allow list' do
      specify do
        expect { write_table }.to output(/Approved/).to_stdout
      end
    end
  end

  describe 'db_outdated?' do
    subject(:db_outdated?) { described_class.db_outdated?(last_updated) }

    context 'when last_updated has not crossed the threshold' do
      let(:last_updated) { (Time.now - (24 * 3600)).to_datetime.to_s }

      it 'returns false' do
        expect(Gcs.logger).to \
          receive(:info).with(match(/It has been 24 hours since the vulnerability database was last updated/))
        expect(db_outdated?).to be_falsy
      end
    end

    context 'when last_updated has crossed the threshold' do
      let(:last_updated) { (Time.now - (72 * 3600)).to_datetime.to_s }

      it 'returns true' do
        expect(Gcs.logger).to \
          receive(:info).with(match(/It has been 72 hours since the vulnerability database was last updated/))
        expect(db_outdated?).to be_truthy
      end
    end

    context 'when last_updated is unknown' do
      let(:last_updated) { 'unknown' }

      it 'returns true' do
        expect(db_outdated?).to be_truthy
      end
    end
  end
end
